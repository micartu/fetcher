//
//  WallViewController.swift
//  LearnFetchRequest
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class WallViewController: BaseTableViewController {
    var output: WallViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Posts"
        output.viewIsReady()
    }
}

// MARK: - WallViewInput
extension WallViewController: WallViewInput {
    func setupInitialState(rightNavButton: UIBarButtonItem?) {
        navigationItem.rightBarButtonItem = rightNavButton
    }
}
