//
//  WallPresenter.swift
//  LearnFetchRequest
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit
import CoreData

final class WallPresenter: FetchResultsPresenter<CPost> {
    weak var view: WallViewInput!
	var interactor: WallInteractorInput! {
		didSet {
			source = interactor
		}
	}
	var retained: UIViewController? = nil
    var router: WallRouterInput!
    private let cache: CachePostServiceProtocol
    
    init(cache: CachePostServiceProtocol,
         fetchedResultsController: NSFetchedResultsController<CPost>,
         converter: CacheConvertionService) {
        self.cache = cache
        super.init(fetchedResultsController: fetchedResultsController,
                   converter: converter)
    }
    
     @objc private func addRecord() {
        view.showInputDialog(title: "Content",
                             message: "Please enter content you want to add") { [weak self] (text) in
                                let id = UUID().uuidString
                                let p = Post(id: id,
                                             name: id,
                                             content: text,
                                             date: Date())
                                self?.cache.create(posts: [p]) {
                                    print("!added: \(p)") // TODO: delete me
                                }
        }
    }
}

// MARK: - WallModuleInput
extension  WallPresenter: WallModuleInput {
	func present(from viewController: UIViewController) {
        viewController.navigationController?
            .pushViewController(view.viewController, animated: true)
        retained = nil
	}
}

// MARK: - WallViewOutput
extension  WallPresenter: WallViewOutput {
    func viewIsReady() {
        let item = UIBarButtonItem(barButtonSystemItem: .add,
                                   target: self,
                                   action: #selector(addRecord))
        view.setupInitialState(rightNavButton: item)
    }
}

// MARK: - WallPostCellDelegate
extension  WallPresenter: WallPostCellDelegate {
    func avatarTouchedOf(id: String) {
        
    }
    
    func likeTouchedOf(id: String) {
        
    }
    
    func seeCommentsOf(id: String) {
        
    }
    
    func addCommentsFor(id: String) {
        
    }
    
    func additionalActionFor(id: String) {
        
    }
    
    func bookmarkTouchedOf(id: String) {
        
    }
    
    func update(id: String) {
        
    }
}

// MARK: - WallInteractorOutput
extension  WallPresenter: WallInteractorOutput {
}
