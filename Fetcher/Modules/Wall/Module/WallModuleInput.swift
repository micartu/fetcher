//
//  WallModuleInput.swift
//  LearnFetchRequest
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

protocol WallModuleInput {
    //var output: WallModuleOutput? { get set }
    func present(from viewController: UIViewController)
}
