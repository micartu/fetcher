//
//  WallModule.swift
//  LearnFetchRequest
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit
import CoreData

final class WallModule {
    class func create() -> WallModuleInput {
        let router = WallRouter()

		let viewController = WallViewController()
		viewController.manualBuilding = true
		viewController.initializer = { tableview in
            let identifier = String(describing: WallPostCell.self)
            tableview.register(UINib(nibName: identifier, bundle: nil),
                               forCellReuseIdentifier: identifier)
			//tableview.allowsSelection = false
		}

        let fetcher: CacheFetchRequestsService = ServicesAssembler.inject()
        let presenter = WallPresenter(cache: ServicesAssembler.inject(),
                                      fetchedResultsController: fetcher.fetchControllerForPost(),
                                      converter: ServicesAssembler.inject())
		viewController.viewModel = presenter
		presenter.retained = viewController
        presenter.view = viewController
        presenter.router = router

        let interactor = WallInteractor(imgLoader: ServicesAssembler.inject())
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter

        return presenter
    }
}
