//
//  IntroInteractor.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class IntroInteractor {
    weak var output: IntroInteractorOutput?
}

// MARK: - IntroInteractorInput
extension IntroInteractor: IntroInteractorInput  {
}
