//
//  IntroModule.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class IntroModule {
    class func create() -> IntroModuleInput {
        let router = IntroRouter()
        
        let viewController = IntroViewController.create()

        let presenter = IntroPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = IntroInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter

        return presenter
    }
}
