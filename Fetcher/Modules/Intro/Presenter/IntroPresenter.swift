//
//  IntroPresenter.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class IntroPresenter {
    weak var view: IntroViewInput!
    var interactor: IntroInteractorInput!
    var router: IntroRouterInput!
}

// MARK: - IntroModuleInput
extension IntroPresenter: IntroModuleInput {
    func install(in window: UIWindow) {
        let nav = UINavigationController(rootViewController: view.viewController)
        nav.isNavigationBarHidden = false
        window.rootViewController = nav
    }
}

// MARK: - IntroViewOutput
extension IntroPresenter: IntroViewOutput {
    func viewIsReady() {
    }    
    
    func showCachedPosts() {
        router.showCachedPosts(from: view.viewController)
    }
    
    func showNetPosts() {
        router.showNetPosts(from: view.viewController)
    }
}

// MARK: - IntroInteractorOutput
extension IntroPresenter: IntroInteractorOutput {
}
