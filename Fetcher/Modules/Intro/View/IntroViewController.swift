//
//  IntroViewController.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class IntroViewController: BaseViewController {
    var output: IntroViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Intro"
        output.viewIsReady()
    }   
    
    @IBAction func showOtherPostsAction(_ sender: Any) {
        output.showCachedPosts()
    }
    
    @IBAction func showNetPostsAction(_ sender: Any) {
        output.showNetPosts()
    }
}

// MARK: - IntroViewInput
extension IntroViewController: IntroViewInput {
    func setupInitialState() {
    }
}

// MARK: - ViewControllerable
extension IntroViewController: ViewControllerable {
    static var storyBoardName: String {
        return "Intro"
    }
}
