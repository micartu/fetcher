//
//  IntroRouterInput.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

protocol IntroRouterInput {    
    func showCachedPosts(from vc: UIViewController)
    func showNetPosts(from vc: UIViewController)
}
