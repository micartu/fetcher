//
//  FakePostsNetwork.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 27/11/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

func generateFakePosts(idStart: Int, count: Int, dateStart: TimeInterval) -> [Post] {
    return (idStart..<(idStart + count))
        .map({
            Post(id: "\($0)",
                 name: "Fake POST #\($0)",
                 content: "Content of Fake Post #\($0)",
                 date: Date(timeIntervalSince1970: dateStart - TimeInterval($0)))
        })
}

final class FakePostsNetwork {
    var recordsOffset = 0
    
    func loadDataFor(page: Int, count: Int) -> [Post] {
        let kDate: TimeInterval = 453381071
        let id = recordsOffset + page * count + 1
        return generateFakePosts(idStart: id,
                                 count: count,
                                 dateStart: kDate)
    }
}
