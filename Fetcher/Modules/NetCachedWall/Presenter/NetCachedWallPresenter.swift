//
//  NetCachedWallPresenter.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class NetCachedWallPresenter: FetchedNetPresenter {
    weak var view: NetCachedWallViewInput!
	var interactor: NetCachedWallInteractorInput! {
		didSet {
			netInteractor = interactor
		}
	}
	var retained: UIViewController? = nil
    var router: NetCachedWallRouterInput!
    
    @objc private func addRecord() {
        view.showInputDialog(title: "Input digit from [0; +Infinity)",
                             message: "Please enter an offset you want to set to our fake network") { [weak self] (text) in
                                let offset = text.toInt
                                if offset >= 0 {
                                    self?.interactor.change(offset: offset)
                                }
        }
    }
}

// MARK: - NetCachedWallModuleInput
extension  NetCachedWallPresenter: NetCachedWallModuleInput {
	func present(from viewController: UIViewController) {
        viewController.navigationController?
            .pushViewController(view.viewController, animated: true)
        retained = nil
	}
}

// MARK: - NetCachedWallViewOutput
extension  NetCachedWallPresenter: NetCachedWallViewOutput {
    func viewIsReady() {
        let item = UIBarButtonItem(barButtonSystemItem: .add,
                                   target: self,
                                   action: #selector(addRecord))
        view.setupInitialState(rightNavButton: item)
    }
}

// MARK: - NetCachedWallPostCellDelegate
extension  NetCachedWallPresenter: WallPostCellDelegate {
    func avatarTouchedOf(id: String) {
    }

    func likeTouchedOf(id: String) {

    }

    func seeCommentsOf(id: String) {

    }

    func addCommentsFor(id: String) {

    }

    func additionalActionFor(id: String) {

    }

    func bookmarkTouchedOf(id: String) {

    }

    func update(id: String) {

    }
}

// MARK: - NetCachedWallInteractorOutput
extension  NetCachedWallPresenter: NetCachedWallInteractorOutput {
}
