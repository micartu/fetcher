//
//  NetCachedWallInteractorInput.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

protocol NetCachedWallInteractorInput: FetchedNetInteractorInput {
    var output: NetCachedWallInteractorOutput? { get set }
    func change(offset: Int)
}
