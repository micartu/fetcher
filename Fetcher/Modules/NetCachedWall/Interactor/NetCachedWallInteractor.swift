//
//  NetCachedWallInteractor.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class NetCachedWallInteractor {
    weak var output: NetCachedWallInteractorOutput?
    private let imgLoader: ImageLoaderProtocol
    private let net: FakePostsNetwork
    private let cache: CachePostServiceProtocol
    
    init(imgLoader: ImageLoaderProtocol,
         cache: CachePostServiceProtocol) {
        self.cache = cache
        self.imgLoader = imgLoader
        net = FakePostsNetwork()
        net.recordsOffset = const.defaultOffset
    }
    
    private struct const {
        static let defaultOffset = 100
    }
}

// MARK: - NetCachedWallInteractorInput
extension NetCachedWallInteractor: NetCachedWallInteractorInput {
    func change(offset: Int) {
        net.recordsOffset = offset
    }
    
    func wrap(entity: Any, with theme: Theme) -> CellAnyModel {
        if let o = entity as? Post {
            return WallPostModel(theme: theme,
                                 id: o.id,
                                 avatar: "",
                                 title: o.name,
                                 place: nil,
                                 images: [],
                                 likeActive: false,
                                 likesCount: 0,
                                 commentCount: 0,
                                 content: o.content,
                                 creationDate: "",
                                 accountId: 0,
                                 bookmarked: false,
                                 delegate: output,
                                 imgLoader: imgLoader)
        }
        else {
            fatalError("NetCachedWallInteractor: Unknown entity: \(entity)")
        }
    }
    
    func loadOverNetwork(page: Int, count: Int,
                         completion: @escaping (([SyncItemProtocol], NSError?) -> Void)) {
        let data = net.loadDataFor(page: page, count: count)
        completion(data, nil)
    }
    
    func fetchEntities(page: Int, count: Int,
                       completion: @escaping (([SyncItemProtocol], NSError?) -> Void)) {
        let p = page - 1 // our page starts from 0 (but in 'network' it starts from 1)!
        let data = cache.postsSubscribedBy(login: "", batchSize: count, step: p)
        completion(data, nil)
    }
    
    // MARK: ItemsToSyncActionsProtocol
    
    func insert(_ item: SyncItemProtocol,
                at index: Int,
                completion: @escaping (() -> Void)) {
        if let p = item as? Post {
            cache.create(posts: [p], completion: completion)
        }  else {
            completion()
        }
    }
    
    func replace(_ item: SyncItemProtocol,
                 at index: Int,
                 completion: @escaping (() -> Void)) {
        if let p = item as? Post {
            cache.update(posts: [p], completion: completion)
        } else {
            completion()
        }
    }
    
    func remove(_ item: SyncItemProtocol,
                completion: @escaping (() -> Void)) {
        if let p = item as? Post {
            cache.delete(postIds: [p.id], completion: completion)
        } else {
            completion()
        }
    }
}
