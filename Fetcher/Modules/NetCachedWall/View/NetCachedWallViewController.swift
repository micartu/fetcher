//
//  NetCachedWallViewController.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class NetCachedWallViewController: BaseTableViewController {
    var output: NetCachedWallViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Fake Networked"
        output.viewIsReady()
    }
}

// MARK: - NetCachedWallViewInput
extension NetCachedWallViewController: NetCachedWallViewInput {
    func setupInitialState(rightNavButton: UIBarButtonItem?) {
        navigationItem.rightBarButtonItem = rightNavButton
    }
}
