//
//  NetCachedWallModule.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class NetCachedWallModule {
    class func create() -> NetCachedWallModuleInput {
        let router = NetCachedWallRouter()

		let viewController = NetCachedWallViewController()
		viewController.manualBuilding = true
		viewController.initializer = { tableview in
            let identifier = String(describing: WallPostCell.self)
            tableview.register(UINib(nibName: identifier, bundle: nil),
                               forCellReuseIdentifier: identifier)			
		}

        let fetcher: CacheFetchRequestsService = ServicesAssembler.inject()
        let presenter = NetCachedWallPresenter(manager: fetcher.fetchDataControllerForPost(),
                                               batchSize: 20, timeOut: 1)
		viewController.viewModel = presenter
		presenter.retained = viewController
        presenter.view = viewController
        presenter.router = router

        let interactor = NetCachedWallInteractor(imgLoader: ServicesAssembler.inject(),
                                                 cache: ServicesAssembler.inject())
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter

        return presenter
    }
}
