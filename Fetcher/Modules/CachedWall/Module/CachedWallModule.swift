//
//  CachedWallModule.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class CachedWallModule {
    class func create() -> CachedWallModuleInput {
        let router = CachedWallRouter()

		let viewController = CachedWallViewController()
		viewController.manualBuilding = true
        viewController.addRefreshControl = false
		viewController.initializer = { tableview in
            let identifier = String(describing: WallPostCell.self)
            tableview.register(UINib(nibName: identifier, bundle: nil),
                               forCellReuseIdentifier: identifier)
		}

        let fetcher: CacheFetchRequestsService = ServicesAssembler.inject()
        let presenter = CachedWallPresenter(manager: fetcher.fetchDataControllerForPost(),
                                            cache: ServicesAssembler.inject())
		viewController.viewModel = presenter
		presenter.retained = viewController
        presenter.view = viewController
        presenter.router = router

        let interactor = CachedWallInteractor(imgLoader: ServicesAssembler.inject())
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter

        return presenter
    }
}
