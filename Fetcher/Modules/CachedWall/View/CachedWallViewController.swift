//
//  CachedWallViewController.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class CachedWallViewController: BaseTableViewController {
    var output: CachedWallViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Posts"
        output.viewIsReady()
    }
}

// MARK: - CachedWallViewInput
extension CachedWallViewController: CachedWallViewInput {
    func setupInitialState(rightNavButton: UIBarButtonItem?) {
        navigationItem.rightBarButtonItem = rightNavButton
    }
}
