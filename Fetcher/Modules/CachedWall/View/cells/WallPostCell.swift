//
//  WallPostCell.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 12.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

protocol WallPostCellDelegate: class {
    func avatarTouchedOf(id: String)
    func likeTouchedOf(id: String)
    func seeCommentsOf(id: String)
    func addCommentsFor(id: String)
    func additionalActionFor(id: String)
    func bookmarkTouchedOf(id: String)
    func update(id: String)
}

final class WallPostCell: UITableViewCell {
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var btnAdditinalActions: UIButton!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var imageComments: UIImageView!
    @IBOutlet weak var lblCommentsCount: UILabel!
    @IBOutlet weak var imagePostTime: UIImageView!
    @IBOutlet weak var lblPostTime: UILabel!
    @IBOutlet weak var lblContents: UILabel!
    @IBOutlet weak var btnSeeAllComments: UIButton!
    @IBOutlet weak var btnAddComment: UIButton!
    @IBOutlet weak var bookmarkView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // register handlers for title and avatar taps:
        for v in [lblTitle, avatarView] {
            let avtap = UITapGestureRecognizer(target: self,
                                               action: #selector(titleTapped))
            v?.addGestureRecognizer(avtap)
            v?.isUserInteractionEnabled = true
        }
    }

    @objc internal func titleTapped() {
        delegate?.avatarTouchedOf(id: id)
    }

    @IBAction func heartTouchedAction(_ sender: Any) {
        delegate?.likeTouchedOf(id: id)
    }

    @IBAction func seeCommentsAction(_ sender: Any) {
        delegate?.seeCommentsOf(id: id)
    }

    @IBAction func addCommentAction(_ sender: Any) {
        delegate?.addCommentsFor(id: id)
    }

    @IBAction func additionalAction(_ sender: Any) {
        delegate?.additionalActionFor(id: id)
    }

    @IBAction func touchBookmarkAction(_ sender: Any) {
        delegate?.bookmarkTouchedOf(id: id)
    }

    var id = ""
    weak var delegate: WallPostCellDelegate? = nil
}
