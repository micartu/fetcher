//
//  CachedWallPresenter.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class CachedWallPresenter: FetchedDataPresenter {
    weak var view: CachedWallViewInput!
	var interactor: CachedWallInteractorInput! {
		didSet {
			source = interactor
		}
	}
	var retained: UIViewController? = nil
    var router: CachedWallRouterInput!
    private let cache: CachePostServiceProtocol
    
    init(manager: FetchManagerProtocol, cache: CachePostServiceProtocol) {
        self.cache = cache
        super.init(manager: manager)
    }
    
    @objc private func addRecord() {
        view.showInputDialog(title: "Content",
                             message: "Please enter content you want to add") { [weak self] (text) in
                                let id = UUID().uuidString
                                let p = Post(id: id,
                                             name: id,
                                             content: text,
                                             date: Date())
                                self?.cache.create(posts: [p]) {
                                    print("!added: \(p)") // TODO: delete me
                                }
        }
    }
    
    override func model(for indexPath: IndexPath) -> Any {
        
        return super.model(for: indexPath)
    }
}

// MARK: - CachedWallModuleInput
extension  CachedWallPresenter: CachedWallModuleInput {
	func present(from viewController: UIViewController) {
        viewController.navigationController?
            .pushViewController(view.viewController, animated: true)
        retained = nil
	}
}

// MARK: - CachedWallViewOutput
extension  CachedWallPresenter: CachedWallViewOutput {
    func viewIsReady() {
        let item = UIBarButtonItem(barButtonSystemItem: .add,
                                   target: self,
                                   action: #selector(addRecord))
        view.setupInitialState(rightNavButton: item)
    }
}

// MARK: - CachedWallPostCellDelegate
extension  CachedWallPresenter: WallPostCellDelegate {
    func avatarTouchedOf(id: String) {
    }
    
    func likeTouchedOf(id: String) {
        
    }
    
    func seeCommentsOf(id: String) {
        
    }
    
    func addCommentsFor(id: String) {
        
    }
    
    func additionalActionFor(id: String) {
        
    }
    
    func bookmarkTouchedOf(id: String) {
        
    }
    
    func update(id: String) {
        
    }
}

// MARK: - CachedWallInteractorOutput
extension  CachedWallPresenter: CachedWallInteractorOutput {
}
