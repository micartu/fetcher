//
//  WallPostModel.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 12.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

protocol WallPostCellModel: CommonCell { }

struct WallPostModel: WallPostCellModel {
    let theme: Theme
    let id: String
    let avatar: String
    let title: String
    let place: String?
    let images: [String]
    var likeActive: Bool
    var likesCount: Int
    var commentCount: Int
    let content: String
    let creationDate: String
    let accountId: Int
    var bookmarked: Bool
    weak var delegate: WallPostCellDelegate?
    let imgLoader: ImageLoaderProtocol

    func setup(cell: WallPostCell) {
        cell.id = id
        cell.delegate = delegate

        cell.avatarView.layer.cornerRadius = cell.avatarView.frame.size.width / 2
        cell.avatarView.clipsToBounds = true
        cell.avatarView.tintColor = theme.secondaryTextColor

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.boldFont.withSize(theme.sz.middle2)
        cell.lblTitle.textColor = theme.mainTextColor

        let cnt = NSMutableAttributedString()
        cnt.append(NSAttributedString(string: content,
                                      attributes: [.font: theme.regularFont.withSize(theme.sz.middle4)]))
        cell.lblContents.attributedText = cnt

        cell.lblCommentsCount.text = "\(commentCount)"
        cell.lblCommentsCount.font = theme.boldFont.withSize(theme.sz.small1)

        cell.lblLikeCount.text = "\(likesCount)"
        cell.lblLikeCount.font = theme.boldFont.withSize(theme.sz.small1)

        cell.imageLike.tintColor = likeActive ? theme.warnColor : theme.inactiveColor

        cell.imageComments.tintColor = theme.inactiveColor

        cell.btnAddComment.setTitleColor(theme.secondaryTextColor, for: .normal)
        cell.btnAddComment.setTitle("Add comment".localized, for: .normal)

        cell.btnSeeAllComments.setTitle("See all comments".localized + " (\(commentCount))", for: .normal)
        cell.btnSeeAllComments.setTitleColor(theme.secondaryTextColor, for: .normal)

        cell.bookmarkView.tintColor = bookmarked ? theme.warnColor : theme.inactiveColor
        cell.btnAdditinalActions.tintColor = theme.secondaryTextColor

        let hidePlace: Bool
        if let p = place, p.count > 0 {
            hidePlace = false
            cell.lblPlace.text = p
            cell.lblPlace.textColor = theme.secondaryTextColor
            cell.placeImage.tintColor = theme.secondaryTextColor
        } else {
            hidePlace = true
        }
        cell.placeImage.isHidden = hidePlace
        cell.lblPlace.isHidden = hidePlace

        cell.lblPostTime.text = creationDate
        cell.lblPostTime.textColor = theme.secondaryTextColor
        cell.imagePostTime.tintColor = theme.secondaryTextColor

        // set default images first:
        cell.avatarView.image = UIImage(named: "avatar")
        cell.avatarView.tintColor = theme.inactiveColor
        cell.mainImage.tintColor = theme.inactiveColor
        //cell.mainImage.contentMode = .scaleAspectFit
        cell.mainImage.image = UIImage(named: "no_image")
        // then try to load the new ones
        if avatar.count > 0 {
            if let av = imgLoader.loadOffline(image: avatar) {
                cell.avatarView.image = av
            } else {
                imgLoader.load(image: avatar) { [weak cell] im in
                    if self.isSameCell(cid: cell?.id) {
                        cell?.avatarView.image = im
                    }
                }
            }
        }
        if let image = images.first, image.count > 0 {
            let install = { [weak cell] (im: UIImage, refresh: Bool) in
                if !self.isSameCell(cid: cell?.id) {
                    return
                }
                if let cell = cell {
                    let h = heightFrom(width: cell.mainImage.bounds.width,
                                       proportional: im.size)
                    cell.mainImage.image = im
                    cell.mainImageHeightConstraint.constant = h
                    if refresh {
                        cell.delegate?.update(id: cell.id)
                    }
                }
            }
            if let im = imgLoader.loadOffline(image: image) {
                install(im, false)
            } else {
                imgLoader.load(image: image) { im in
                    install(im, true)
                }
            }
        }
    }

    func isSameCell(cid: String?) -> Bool {
        if cid == id {
            return true
        }
        return false
    }
}
