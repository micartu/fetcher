//
//  CachedWallInteractor.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/10/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class CachedWallInteractor {
    weak var output: CachedWallInteractorOutput?
    private let imgLoader: ImageLoaderProtocol
    init(imgLoader: ImageLoaderProtocol) {
        self.imgLoader = imgLoader
    }
}

extension CachedWallInteractor: CachedWallInteractorInput {
	func loadModels(with theme: Theme) {
	}
    
    func wrap(entity: Any, with theme: Theme) -> CellAnyModel {
        if let o = entity as? Post {
            return WallPostModel(theme: theme,
                                 id: "",
                                 avatar: "",
                                 title: o.name,
                                 place: nil,
                                 images: [],
                                 likeActive: false,
                                 likesCount: 0,
                                 commentCount: 0,
                                 content: o.content,
                                 creationDate: "",
                                 accountId: 0,
                                 bookmarked: false,
                                 delegate: output,
                                 imgLoader: imgLoader)
        }
        else {
            fatalError("CachedWallInteractor: Unknown entity: \(entity)")
        }
    }
    
    func loadOverNetwork(offset: Int,
                         count: Int,
                         completion: @escaping (([SyncItemProtocol], NSError?) -> Void)) {
    }
    
    func fetchEntities(offset: Int,
                       count: Int,
                       completion: @escaping (([SyncItemProtocol], NSError?) -> Void)) {
    }
}
