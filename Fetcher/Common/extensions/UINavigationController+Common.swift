//
//  UINavigationController + Common.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 10/06/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

extension UINavigationController {
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		return topViewController?.preferredStatusBarStyle ?? .default
	}
	
	static func present(from vc: UIViewController, rootVC: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
		let nvc = UINavigationController(rootViewController: rootVC)
		if Const.Devices.iPad {
			nvc.modalPresentationStyle = .formSheet
			nvc.preferredContentSize = Const.Sizes.iPadContentSize
		}
		vc.present(nvc, animated: animated, completion: completion)
	}
}
