//
//  String+Localization.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
