//
//  FetchedDataPresenter.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 18/11/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol FetchedDataInteractorInput: class {
    func wrap(entity: Any, with theme: Theme) -> CellAnyModel    
}

protocol FetchedDataInteractorOutput: class {
}

class FetchedDataPresenter: CommonViewModel,
    FetchedDataInteractorOutput {
    fileprivate var manager: FetchManagerProtocol
    
    init(manager: FetchManagerProtocol) {
        self.manager = manager
        self.manager.delegate = self
    }
    
    // data source
    weak var source: FetchedDataInteractorInput?
    
    // MARK: FetchResultsInteractorOutput
    
    // MARK: CommonViewModel
    
    func getContents() {
        manager.fetchData()
    }
    
    func refreshContents() {
        // data is already in sync with db, do not worry.
    }
    
    func selected(indexPath: IndexPath) {
    }
    
    func model(for indexPath: IndexPath) -> Any {
        guard let source = source else {
            // TODO: add empty model here!
            fatalError("FetchedDataPresenter: source isn't set!")
        }
        let obj = manager.objectAt(indexPath)
        return source.wrap(entity: obj, with: theme)        
    }
    
    func modelsCount(for section: Int) -> Int {
        return manager.objectsCountFor(section: section)
    }
    
    func title(for section: Int) -> String {
        return ""
    }
    
    func sectionsCount() -> Int {
        // multi sections isn't supported yet!
        return 1
    }
    
    // MARK: members of CommonViewModel protocol
    var theme: Theme! {
        didSet {
            getContents()
        }
    }
    var updateItems = BehaviorRelay<IIndexPathCount?> (value: nil)
    var updateData = BehaviorRelay<Bool> (value: false)
    var scrollTo = BehaviorRelay<IMovePath?> (value: nil)
    var removeKeyboard = BehaviorRelay<Bool> (value: false)
    var busy = BehaviorRelay<Bool> (value: false)
    var batchUpdate = BehaviorRelay<Bool?> (value: nil)
    var make = BehaviorRelay<CellChangeType?> (value: nil)    
}

extension FetchedDataPresenter: FetchManagerDelegate {
    func beginBatchUpdate() {
        // would call tableView.beginUpdates() or equivalent
        batchUpdate.accept(true)        
    }
    
    func make(change: CellChangeType) {
        switch change {
        case .insert(let index):
            make.accept(.insert(index: index))
        case .update(let index):
            make.accept(.update(index: index))
        case .move(let from, let to):
            make.accept(.move(from: from, to: to))
        case .delete(let index):
            make.accept(.delete(index: index))
        }
    }
    
    func endBatchUpdate() {
        batchUpdate.accept(false)
        // switch off batch update
        batchUpdate.accept(nil)        
    }
}
