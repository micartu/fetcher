//
//  FetchedNetPresenter.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24/11/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/// interactor must implement those methods for us in order to function properly
protocol FetchedNetInteractorInput: FetchedDataInteractorInput, ItemsToSyncActionsProtocol {
    func loadOverNetwork(page: Int,
                         count: Int,
                         completion: @escaping (([SyncItemProtocol], NSError?) -> Void))
    func fetchEntities(page: Int,
                       count: Int,
                       completion: @escaping (([SyncItemProtocol], NSError?) -> Void))
}

/// Presenter for loading data over the network and sync it
/// with the cache and show only cached data to user
class FetchedNetPresenter: FetchedDataPresenter {
    var netInteractor: FetchedNetInteractorInput? {
        didSet {
            source = netInteractor
        }
    }
    
    init(manager: FetchManagerProtocol,
         batchSize: Int = 20,
         timeOut: TimeInterval = 5) {
        self.sz = batchSize
        self.timeOut = timeOut
        queue = DispatchQueue(label: "FetchedNetPresenterQueue",
                              qos: .utility)
        syncer = GeneralSynchronizer(statusItems: [],
                                     statusDict: StatusDictType(), // an empty status dictionary
                                     deb: nil) { (items, dict) in
                                        // DISCLAIMER:
                                        // DO NOTHING, WE DO NOT NEED TO DETERMINE DIFFERENCIES
                                        // WITH THE SERVER SIDE WE INTEND
                                        // ONLY TO UPDATE OUR OWN DATABASE!
        }
        super.init(manager: manager)
    }
    
    private func shouldBeSyncedAfter(time: Date?) -> Bool {
        if let tt = time,
            tt.timeIntervalSinceNow > timeOut {
            return true
        } else if time == nil {
            return true
        }
        return false
    }
    
    override func getContents() {
        super.getContents()
        // if we do not have any data, then try to load
        // it from network and merge with the inner db:
        if modelsCount(for: 0) == 0 {
            busy.accept(true)
            loadAndSync(page: 1)
        }
    }
    
    override func refreshContents() {
        topRecordLoadTime = nil
        loadAndSync(page: 1)
    }
    
    override func model(for indexPath: IndexPath) -> Any {
        let mcount = modelsCount(for: 0) // models count for section 0
        if indexPath.row >= mcount * 3 / 4 {
            let toLoad = indexPath.row / sz + 2
            if loaded < toLoad {
                busy.accept(true)
                loadAndSync(page: toLoad)
            }
        } else if indexPath.row == 0 { // first record
            if shouldBeSyncedAfter(time: topRecordLoadTime) {
                topRecordLoadTime = Date()
                loadAndSync(page: 1)
            }
        }
        // get data from fetcher (inner cache):
        return super.model(for: indexPath)
    }
    
    private func loadAndSync(page: Int) {
        let g = DispatchGroup()
        var error: NSError? = nil
        let lock = NSRecursiveLock()
        var netItems = [SyncItemProtocol]()
        var locItems = [SyncItemProtocol]()
        g.enter()
        netInteractor?.loadOverNetwork(page: page, count: sz) { (items, err) in
            if let e = err {
                lock.lock()
                error = e
                lock.unlock()
            }
            else {
                netItems = items
            }
            g.leave()
        }
        g.enter()
        netInteractor?.fetchEntities(page: page, count: sz) { (items, err) in
            if let e = err {
                lock.lock()
                error = e
                lock.unlock()
            }
            else {
                locItems = items
            }
            g.leave()
        }
        g.notify(queue: queue) { [weak self] in
            guard let `self` = self else { return }
            if let e = error {
                print("FetchedNetPresenter: while being in syncing process " +
                    "of data an error occured: \(e.localizedDescription)")
            } else {
                let order = self.syncer.getSortOrder()
                let netToSync = ItemsToSync(items: netItems.sorted(by: order),
                                            interactor: nil) // do not need to sync data over the network
                let locToSync = ItemsToSync(items: locItems.sorted(by: order),
                                            interactor: self.netInteractor)
                self.syncer.sync(A: netToSync, B: locToSync)
                self.busy.accept(false) // hide load indicator if any
            }
        }
    }
    
    /// inner class-wrapper for making a sync process possible
    final class ItemsToSync: ItemsToSyncProtocol {
        var items: [SyncItemProtocol]
        private let interactor: ItemsToSyncActionsProtocol?
                
        init(items: [SyncItemProtocol],
             interactor: ItemsToSyncActionsProtocol?) {
            self.items = items
            self.interactor = interactor
        }
        
        func insert(_ item: SyncItemProtocol,
                    at index: Int,
                    completion: @escaping (() -> Void)) {
            interactor?.insert(item, at: index,
                               completion: completion) ??
                completion()
        }
        
        func replace(_ item: SyncItemProtocol,
                     at index: Int,
                     completion: @escaping (() -> Void)) {
            interactor?.replace(item, at: index,
                                completion: completion) ??
                completion()
        }
        
        func remove(_ item: SyncItemProtocol,
                    completion: @escaping (() -> Void)) {
            interactor?.remove(item, completion: completion) ??
                completion()
        }
    }
    
    // MARK: other members
    private var loaded = -1    
    internal var sz: Int
    fileprivate var topRecordLoadTime: Date? = nil
    fileprivate let timeOut: TimeInterval
    fileprivate let queue: DispatchQueue
    fileprivate let syncer: GeneralSynchronizer
}
