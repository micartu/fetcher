//
//  SingleSectionPresenter.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 29/07/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol SingleSectionInteractorInput: class {
    func loadModels(with theme: Theme)
}

protocol SingleSectionInteractorOutput: class {
    func fetched(models: [CellAnyModel])
}

open class SingleSectionPresenter:
   CommonViewModel,
   SingleSectionInteractorOutput {
    // data source
    weak var source: SingleSectionInteractorInput?

    // MARK: SingleSectionInteractorOutput

    func fetched(models: [CellAnyModel]) {
        self.models = models
        busy.accept(false)
        updateData.accept(true)
    }

    // MARK: CommonViewModel

    func getContents() {
        if busy.value || source == nil { return }
        busy.accept(true)
        source?.loadModels(with: theme)
    }

    func refreshContents() {
        getContents()
    }

    func selected(indexPath: IndexPath) {
    }

    func model(for indexPath: IndexPath) -> Any {
        return models[indexPath.row]
    }

    func modelsCount(for section: Int) -> Int {
        return models.count
    }

    func title(for section: Int) -> String {
        return ""
    }

    func sectionsCount() -> Int {
        return 1
    }

    // MARK: useful functions
    internal func findModelWith(id: String) -> (CellAnyModel?, Int) {
        for i in models.indices {
            let m = models[i]
            if m.id == id {
                return (m, i)
            }
        }
        return (nil, -1)
    }

    // MARK: members of CommonViewModel protocol
    var theme: Theme! {
        didSet {
            getContents()
        }
    }
    var updateItems = BehaviorRelay<IIndexPathCount?> (value: nil)
    var updateData = BehaviorRelay<Bool> (value: false)
    var scrollTo = BehaviorRelay<IMovePath?> (value: nil)
    var removeKeyboard = BehaviorRelay<Bool> (value: false)
    var busy = BehaviorRelay<Bool> (value: false)
    var batchUpdate = BehaviorRelay<Bool?> (value: nil)
    var make = BehaviorRelay<CellChangeType?> (value: nil)

    // MARK: other members
    internal var models = [CellAnyModel]()
}
