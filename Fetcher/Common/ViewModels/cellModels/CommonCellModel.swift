//
//  CommonCellModel.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 11/09/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import Foundation

protocol CommonCellModel {
    var id: String { get }
}
