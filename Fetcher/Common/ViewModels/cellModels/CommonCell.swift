//
//  CommonCell.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import UIKit

protocol CommonCell: CellAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

extension CommonCell {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}
