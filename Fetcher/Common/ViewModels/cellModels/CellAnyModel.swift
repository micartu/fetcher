//
//  CellAnyModel.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import UIKit

protocol CellAnyModel: CommonCellModel {
    static var CellAnyType: UIView.Type { get }
    func setupAny(cell: UIView)
}
