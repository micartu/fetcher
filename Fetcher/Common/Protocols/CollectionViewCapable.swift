//
//  CollectionViewCapable.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 18.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

typealias CollectionViewCapable = BaseCollectionViewController & UICollectionViewDelegate & UICollectionViewDataSource
