//
//  CellChangeType.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 22/10/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import UIKit

enum CellChangeType {
    case insert(index: IndexPath)
    case update(index: IndexPath)
    case move(from: IndexPath, to: IndexPath)
    case delete(index: IndexPath)
}
