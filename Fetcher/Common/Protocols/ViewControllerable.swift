//
//  ViewControllerable.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 10/06/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

protocol ViewControllerable: class {
	static var storyBoardName: String { get }
}

extension ViewControllerable where Self: UIViewController {
	static func create() -> Self {
		let storyboard = self.storyboard()
		let className = NSStringFromClass(Self.self)
		let finalClassName = className.components(separatedBy: ".").last!
		
		let viewControllerId = finalClassName
		let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerId)
		
		return viewController as! Self
	}
	
	static func storyboard() -> UIStoryboard {
		return UIStoryboard(name: storyBoardName, bundle: nil)
	}
}
