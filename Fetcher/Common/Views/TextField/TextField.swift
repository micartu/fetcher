//
//  UITextField +.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 15/06/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

class TextField: UITextField {
	let padding = UIEdgeInsets(top: 0, left: 26, bottom: 0, right: 0)
	
	override open func textRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: padding)
	}
	
	override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: padding)
	}
	
	override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
	}	
}

class ToolbarWithDoneButton: UIToolbar {
    private var doneButton: UIBarButtonItem!
    public weak var textField: UITextField?
    private var donePressedBlock: (() -> Void)?
    
    convenience init(frame: CGRect, donePressedBlock: (() -> Void)?) {
        self.init(frame: frame)
        self.donePressedBlock = donePressedBlock
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.donePressed(_:)))
        self.items = [self.doneButton]
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.donePressed(_:)))
        self.items = [self.doneButton]
    }
    
    @objc private func donePressed(_ sender: UIBarButtonItem) {
        self.textField?.resignFirstResponder()
        self.donePressedBlock?()
    }
}

