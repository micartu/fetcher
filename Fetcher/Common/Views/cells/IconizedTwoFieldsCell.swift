//
//  IconizedTwoFieldsCell.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 26.09.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

protocol IconizedTwoFieldsCellDelegate: TouchableCell {
}

final class IconizedTwoFieldsCell: UITableViewCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(cellTapped))
        addGestureRecognizer(tap)
    }

    @objc private func cellTapped() {
        delegate?.cellTouched(id: id)
    }

    var id = ""
    weak var delegate: IconizedTwoFieldsCellDelegate? = nil
}
