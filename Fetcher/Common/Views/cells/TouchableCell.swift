//
//  TouchableCell.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 30/09/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import Foundation

protocol TouchableCell: class {
    func cellTouched(id: String)
}
