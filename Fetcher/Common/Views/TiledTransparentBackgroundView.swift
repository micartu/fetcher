//
//  TiledBackgroundView.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 08/09/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

@IBDesignable
final class TiledTransparentBackgroundView: UIView {
    @IBInspectable
    var sideLength: CGFloat = 50.0 {
        didSet {
            commonInit()
            setNeedsDisplay()
        }
    }

    @IBInspectable
    var oddColor: UIColor = .white {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable
    var evenColor: UIColor = .gray {
        didSet {
            setNeedsDisplay()
        }
    }

    override class var layerClass: AnyClass {
        return CATiledLayer.self
    }

    private func commonInit() {
        let layer = self.layer as! CATiledLayer
        let scale = UIScreen.main.scale
        layer.contentsScale = scale
        layer.tileSize = CGSize(width: sideLength * scale,
                                height: sideLength * scale)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        let x = Int(rect.origin.x / sideLength) % 2
        let y = Int(rect.origin.y / sideLength) % 2
        let c: UIColor = ((x ^ y) == 0) ? oddColor : evenColor
        context?.setFillColor(c.cgColor)
        context?.fill(rect)
    }
}
