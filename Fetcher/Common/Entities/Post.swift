//
//  Post.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 07/10/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

struct Post {
    let id: String
    let name: String
    let content: String
    let date: Date
}
