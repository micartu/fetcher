//
//  TopBarController.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 26.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

enum TopBarType {
    case withContent // appears at the top of content
    case floating // shows up if user scrolls up
    case fixed // is shown always
}

class TopBarController: NSObject, UIScrollViewDelegate, Themeble {
    var barHeight: CGFloat {
        get {
            return bHeight
        }
    }

    // MARK: Life cycle
    func install(bar: UIView, type: TopBarType, height: CGFloat) {
        bHeight = height
        topBar = bar
        self.type = type
        if type == .fixed {
            alwaysVisibleHeight = height
        }
        bar.clipsToBounds = true
    }

    func initialize() {
        topBar.frame = CGRect(x: 0, y: 0,
                              width: UIScreen.main.bounds.size.width,
                              height: bHeight)
    }

    // MARK: UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height: CGFloat
        switch type {
        case .fixed:
            fallthrough
        case .withContent:
            height = calcStaticHeightBasedOn(offset: scrollView.contentOffset.y)
        case .floating:
            let y = scrollView.contentOffset.y
            let delta = -(y - lastY)
            let maxHeight = bHeight
            if curH > 0 && curH <= maxHeight {
                curH += delta
            } else {
                if curH > maxHeight {
                    curH = maxHeight
                } else if delta > 0 {
                    curH += delta
                } else {
                    curH = 0
                }
            }
            height = max(min(curH, maxHeight),
                         calcStaticHeightBasedOn(offset: y))
            // update last y position
            lastY = y
        }
        topBar.frame = CGRect(x: 0, y: 0,
                              width: UIScreen.main.bounds.size.width,
                              height: height)
    }

    var isHidden: Bool {
        get {
            guard topBar != nil else { return true }
            return topBar.isHidden
        }
        set {
            topBar.isHidden = newValue
        }
    }

    // MARK: Themeble
    func apply(theme: Theme) {
        // is top bar themable?
        if let bar = topBar as? Themeble {
            bar.apply(theme: theme)
        } else {
            // or maybe there are some themable siblings?
            for v in topBar.subviews {
                if let bar = v as? Themeble {
                    bar.apply(theme: theme)
                }
            }
        }
    }

    private func calcStaticHeightBasedOn(offset: CGFloat) -> CGFloat {
        let y = bHeight - (offset + bHeight)
        return min(max(y, alwaysVisibleHeight),
                   bHeight + kBarDamping)
    }

    // MARK: constants
    private let kBarDamping: CGFloat = 100

    // MARK: members
    private weak var topBar: UIView!
    private var alwaysVisibleHeight: CGFloat = 0
    private var bHeight: CGFloat = 0
    private var type: TopBarType = .withContent
    private var curH: CGFloat = 0 // current height of the bar in floating mode
    private var lastY: CGFloat = 0 // last used y scroll position
}
