//
//  BaredBaseTableViewController.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 26.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

class BaredBaseTableViewController: BaseTableViewController {
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        topBar.initialize()
        tableview.contentInset = UIEdgeInsets(top: topBar.barHeight, left: 0, bottom: 0, right: 0)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        topBar.scrollViewDidScroll(scrollView)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        scrollViewDidScroll(tableview)
    }

    // MARK: Themeble
    override func apply(theme: Theme) {
        super.apply(theme: theme)
        topBar.apply(theme: theme)
        tableview.separatorColor = theme.separatorColor
    }

    // MARK: members
    internal var topBar = TopBarController()
}
