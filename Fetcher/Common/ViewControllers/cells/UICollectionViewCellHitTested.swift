//
//  UICollectionViewCellHitTested.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 17.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

class UICollectionViewCellHitTested: UICollectionViewCell {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        for i in stride(from: subviews.count - 2, to: 0, by: -1) {
            let newPoint = subviews[i].convert(point, from: self)
            let view = subviews[i].hitTest(newPoint, with: event)
            if view != nil{
                return view
            }
        }
        return super.hitTest(point, with: event)
    }
}
