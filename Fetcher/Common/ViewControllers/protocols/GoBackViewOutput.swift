//
//  GoBackViewOutput.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 12/09/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import Foundation

protocol GoBackViewOutput: class {
    func movedBackAction()
}
