//
//  functions.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import UIKit
import STPopup

func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

func runOnMainThread(_ closure: @escaping ()->()) {
    if Thread.isMainThread {
        closure()
    } else {
        DispatchQueue.main.async {
            closure()
        }
    }
}

func threadId() -> String {
    return Thread.current.debugDescription
}

func applicationVersion() -> String {
    if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
        var v = " \(version)"
        if let bv = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            v += ".\(bv)"
        }
        return v
    }
    else {
        return ""
    }
}

func defaultDescriptionFrom(dict: NSDictionary) -> String {
    if let en = dict["en"] as? String { // a default value
        return en
    }
    return ""
}

func localizedDescriptionFrom(dict: NSDictionary) -> String {
    if let locale = NSLocale.current.languageCode {
        if let localized = dict[locale.lowercased()] as? String {
            return localized
        } else {
            return defaultDescriptionFrom(dict: dict)
        }
    }
    return ""
}

func localizedDescriptionFrom(dict: NSDictionary, field: String) -> String {
    if let d = dict[field] as? String {
        return d
    } else if let d = dict[field] as? [String] {
        return d.joined(separator: "\n")
    } else if let d = dict[field] as? NSDictionary {
        return localizedDescriptionFrom(dict: d)
    }
    return ""
}

func appendErrorMessagesFrom(_ dict: NSDictionary,
                             error: String,
                             field: String) -> (String, Bool) {
    let v = localizedDescriptionFrom(dict: dict, field: field)
    if v.count > 0 {
        return (error + "\n" + v, true)
    }
    return (error, false)
}

func formErrorMessageFrom(_ error: NSError) -> String {
    var descr = error.localizedDescription
    if let infos = error.userInfo[Const.err.fields.data] as? [NSDictionary] {
        for info in infos {
            for (k, _) in info {
                if let f = k as? String {
                    (descr, _) = appendErrorMessagesFrom(info, error: descr, field: f)
                }
            }
        }
    }
    return descr
}

func applyRoundedCornersTo(view: UIView,
                           corners: UIRectCorner,
                           r: CGSize,
                           offset: UIEdgeInsets = UIEdgeInsets.zero) {
    let maskLayer = CAShapeLayer()
    let maskPath = UIBezierPath(roundedRect: view.bounds.inset(by: offset),
                                byRoundingCorners: corners,
                                cornerRadii: r)
    maskLayer.frame = view.bounds
    maskLayer.path = maskPath.cgPath
    view.layer.mask = maskLayer
    view.layer.masksToBounds = true
}

func imageWith(image:UIImage, scaledToSize newSize:CGSize, scale: CGFloat = 0.0) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(newSize, false, scale)
    image.draw(in: CGRect(origin: CGPoint.zero,
                          size: CGSize(width: newSize.width, height: newSize.height)))
    let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return newImage
}

func maskFrom(transparency: UIImage, rgb: UIImage) -> UIImage {
    let sz = rgb.size
    guard sz.width > 0 && sz.height > 0 else { return UIImage() }
    UIGraphicsBeginImageContextWithOptions(sz, false, 0.0)
    rgb.draw(at: CGPoint.zero)
    transparency.draw(at: CGPoint.zero, blendMode: .destinationIn, alpha: 1)
    let mask:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return mask
}

func mixImageFrom(source:UIImage, mask: UIImage, transparency: UIImage, rgb: UIImage) -> UIImage {
    let sz = rgb.size
    let maskedNew = maskFrom(transparency: transparency, rgb: rgb)
    UIGraphicsBeginImageContextWithOptions(sz, false, 0.0)
    source.draw(in: CGRect(origin: CGPoint.zero,
                           size: CGSize(width: sz.width, height: sz.height)))
    maskedNew.draw(at: CGPoint.zero, blendMode: .destinationIn, alpha: 1)
    let masked:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return masked
}

@inline(__always)
func heightFrom(width: CGFloat, proportional sz: CGSize) -> CGFloat {
    assert(sz.width > 0.001)
    return sz.height / sz.width * width
}

@inline(__always)
func widthFrom(height: CGFloat, proportional sz: CGSize) -> CGFloat {
    assert(sz.height > 0.001)
    return sz.width / sz.height * height
}

func generatePopUp(for vc: UIViewController,
                   yfactor: CGFloat = 0.8,
                   xfactor: CGFloat = 0.8) -> STPopupController {
    return generatePopUp(for: vc,
                         width: -1,
                         height: -1,
                         xfactor: xfactor,
                         yfactor: yfactor)
}

private func getMeasureFor(value: CGFloat, lessThan: CGFloat) -> CGFloat {
    if value > lessThan || value < 0 {
        return lessThan
    }
    return value
}

func generatePopUp(for vc: UIViewController,
                   width: CGFloat,
                   height: CGFloat,
                   xfactor: CGFloat = 0.8,
                   yfactor: CGFloat = 0.8) -> STPopupController {
    let popUp = STPopupController(rootViewController: vc)
    popUp.style = .formSheet
    popUp.transitionStyle = .fade
    let sz = UIScreen.main.bounds.size
    popUp.containerView.layer.cornerRadius = 8
    let widthPortrait = getMeasureFor(value: width, lessThan: sz.width * xfactor)
    let heightPortrait = getMeasureFor(value: height, lessThan: sz.height * yfactor)
    vc.contentSizeInPopup = CGSize(width: widthPortrait,
                                   height: heightPortrait)
    let widthLand = getMeasureFor(value: height, lessThan: sz.height * xfactor)
    let heighLand = getMeasureFor(value: width, lessThan: sz.width * yfactor)
    vc.landscapeContentSizeInPopup = CGSize(width: widthLand,
                                            height: heighLand)
    return popUp
}

func open(link: String) {
    if link.contains("http") {
        if let url = URL(string: link) {
            UIApplication.shared.open(url, options: [:])
        }
    }
}
