//
//  CoreDataInit.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//


import UIKit
import MagicalRecord

final class CoreDataInit {
    private var dbStore : String {
        get {
            return "Fetcher"
        }
    }

    func setupCoreDataStack() {
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: dbStore)
    }

    func cleanUp() {
        MagicalRecord.cleanUp()

        var removeError: NSError?
        let deleteSuccess: Bool
        do {
            guard let url = NSPersistentStore.mr_url(forStoreName: dbStore) else {
                return
            }
            let p = url.path
            let walUrl = URL(string: p + "-wal") ?? url
            let shmUrl = URL(string: p + "-shm") ?? url

            try FileManager.default.removeItem(at: url)
            try? FileManager.default.removeItem(at: walUrl)
            try? FileManager.default.removeItem(at: shmUrl)

            deleteSuccess = true
        } catch let error as NSError {
            removeError = error
            deleteSuccess = false
        }

        if deleteSuccess {
            print(">>> db: '\(dbStore)' successfully deleted, create and initialize a new one...")
        } else {
            print(">>> An error has occured while deleting '\(dbStore)'")
            print(">>> Error description: \(removeError.debugDescription)")
        }
        setupCoreDataStack()
    }
}

extension CoreDataInit: AppDelegateProtocol {
    func description() -> String {
        return "CoreDataInit"
    }

    func initModule(application: UIApplication,
                    options: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let kVersion = "version_key"
        let kCleaned = "cleaned"
        let def = UserDefaults.standard
        let writeCleanedInfo: ((NSDictionary, Bool) -> Void) = { (d, cleaned) in
            let dict = d.mutableCopy() as! NSMutableDictionary
            dict[kVersion] = applicationVersion()
            dict[kCleaned] = cleaned
            def.set(dict, forKey: Const.cache.info)
            def.synchronize()
        }
        if let info = def.dictionary(forKey: Const.cache.info) {
            let cleaned: Bool
            if let c = info[kCleaned] as? Bool {
                cleaned = c
            } else {
                cleaned = false
            }
            var needToReinit = false
            if let v = info[kVersion] as? String {
                let vv = v.split(separator: ".").map({ String($0).toInt })
                if vv.count >= 4 {
                    // recreate db in version 0.7.0.13 but only in branch 0.7.x
                    if vv[0] == 0 &&
                        vv[1] == 7 &&
                        vv[2] >= 0 &&
                        vv[3] >= 13 && !cleaned {
                        needToReinit = true
                    }
                }
            }
            if needToReinit {
                writeCleanedInfo(info as NSDictionary, true)
                cleanUp()
            } else {
                setupCoreDataStack()
            }
        } else {
            writeCleanedInfo(NSDictionary(), true)
            cleanUp()
        }
        return true
    }

    var deinitModule: ((UIApplication) -> Void)? {
        return { [weak self] app in
            self?.saveContext()
        }
    }

    var backgroundMethod: ((UIApplication) -> Void)? {
        return { [weak self] a in
            self?.saveContext()
        }
    }

    private func saveContext() {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStore(completion: { (success, err) in
            if let e = err {
                print("error while saving core data: \(e.localizedDescription)")
            }
        })
    }
}
