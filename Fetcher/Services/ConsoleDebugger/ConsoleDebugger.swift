//
//  ConsoleDebugger.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 28.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

final public class ConsoleDebugger: ConsoleDebuggerService {
    public init(featureManager: FeatureManagerService) {
        featureManager.append(listener: self)
    }

    public func set(name: String) {
        self.dname = name
    }

    public func pr(_ messages: String...) {
        if enabled {
            var out = [String]()
            out.append(contentsOf: messages)
            let title = "[\(dname)]: "
            out.insert(title, at: 0)
            print(out.joined())
        }
    }

    // MARK: - Private

    private var dname = ""
    private var enabled = false
}

extension ConsoleDebugger: FeatureListenerService {
    public func toggled(enabled: Bool) {
        self.enabled = enabled
    }

    public func name() -> String {
        return "ConsoleDebugger"
    }
}
