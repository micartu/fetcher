//
//  ServicesAssembler.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 23.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol ServiceLocatorModulProtocol {
    func registerServices(serviceLocator: ServicesAssembler)
}

final class ServicesAssembler {
    init() {
        let secrets = Secret()
        registerSingleton(singletonInstance: secrets as SecretServiceProtocol)

        let tm = ThemeManager()
        registerSingleton(singletonInstance: tm as ThemeManagerProtocol)
        registerSingleton(singletonInstance: tm as ThemebleRouting)

        let cfgName: String
        #if DEBUG
        cfgName = "network.debug"
        #else
        cfgName = "network"
        #endif
        
        let net = NetFun()        
        registerSingleton(singletonInstance: net as NetPostsService)
        
        let cache = Cache(secret: secrets)        
        registerSingleton(singletonInstance: cache as CachePostServiceProtocol)
        registerSingleton(singletonInstance: cache as CacheFetchRequestsService)
        registerSingleton(singletonInstance: cache as CacheConvertionService)
        /*
        registerSingleton(singletonInstance: cache as CacheExecutionService)
        registerSingleton(singletonInstance: cache as CacheUserServiceProtocol)
        registerSingleton(singletonInstance: cache as CacheCommentServiceProtocol)
        registerSingleton(singletonInstance: cache as CachePhotoServiceProtocol)
        registerSingleton(singletonInstance: cache as CachePlaceServiceProtocol)
        registerSingleton(singletonInstance: cache as CacheStoryServiceProtocol)
        registerSingleton(singletonInstance: cache as CacheGaleryService)
        */
        
        let filesaver = FileSaver()
        registerSingleton(singletonInstance: filesaver as FileSaverProtocol)
        register(factory: {
            ImageLoader(disk: filesaver) as ImageLoaderProtocol
        })
    }

    // MARK: Registration

    public func register<Service>(factory: @escaping () -> Service) {
        let serviceId = ObjectIdentifier(Service.self)
        registry[serviceId] = factory
    }

    public static func register<Service>(factory: @escaping () -> Service) {
        sharedServices.register(factory: factory)
    }

    public func registerSingleton<Service>(singletonInstance: Service) {
        let serviceId = ObjectIdentifier(Service.self)
        registry[serviceId] = singletonInstance
    }

    public static func registerSingleton<Service>(singletonInstance: Service) {
        sharedServices.registerSingleton(singletonInstance: singletonInstance)
    }

    public func registerModules(modules: [ServiceLocatorModulProtocol]) {
        modules.forEach { $0.registerServices(serviceLocator: self) }
    }

    public static func registerModules(modules: [ServiceLocatorModulProtocol]) {
        sharedServices.registerModules(modules: modules)
    }

    // MARK: Injection

    public static func inject<Service>() -> Service {
        return sharedServices.inject()
    }

    // MARK: private

    private func inject<Service>() -> Service {
        let serviceId = ObjectIdentifier(Service.self)
        if let factory = registry[serviceId] as? () -> Service {
            return factory()
        } else if let singletonInstance = registry[serviceId] as? Service {
            return singletonInstance
        } else {
            fatalError("No registered entry for \(Service.self)")
        }
    }

    private var registry = [ObjectIdentifier:Any]()
    private static var sharedServices: ServicesAssembler = {
        let shared = ServicesAssembler()
        return shared
    }()
}
