//
//  Cache+Convert.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 21/10/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CacheConvertionService {
    func convert(object: Any) -> Any {
        if let co = object as? CPost {
            return convert(post: co)
        }
        fatalError("Cache: cannot find for core data object: \(object) an equivalent entity")
    }
}
