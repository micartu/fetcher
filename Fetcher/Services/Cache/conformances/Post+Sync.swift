//
//  Post+Sync.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 27/11/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

extension Post: SyncItemProtocol {
    public var itemID: String {
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US_POSIX")
        df.dateFormat = "yyyy-MM-dd HH:mm:ssZZZ"
        return df.string(from: date) + ";" + id
    }
    
    public var tag: String {
        return id + content
    }
}
