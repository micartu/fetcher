//
//  Cache+Post.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 11.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import Foundation
import MagicalRecord
import CoreData

extension Cache: CachePostServiceProtocol {
    func postExistsWith(id: String) -> Bool {
        return existsPostId(id, in: getContext())
    }

    func getPostWith(id: String) -> Post? {
        if let cp = getCPostWithId(id) {
            return convert(post: cp)
        }
        return nil
    }

    func postsSubscribedBy(login: String,
                           batchSize: Int,
                           step: Int) -> [Post] {
        var pp = [Post]()
        if let cps = postsSubscribedBy(login: login,
                                       batchSize: batchSize,
                                       step: step,
                                       in: getContext()) {
            for cp in cps {
                let p = convert(post: cp)
                pp.append(p)
            }
        }
        return pp
    }

    func create(posts: [Post], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for p in posts {
                self.createOrUpdateCPost(p, in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    func update(posts: [Post], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for p in posts {
                self.createOrUpdateCPost(p, in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    func delete(postIds: [String], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for p in postIds {
                self.delete(postId: p, in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }
    
    func deleteAllPosts(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let posts = CPost.mr_findAll(in: context) as? [CPost] {
                for p in posts {
                    self.delete(cpost: p, in: context)
                }
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    // MARK: - Internal

    internal func existsPostId(_ id: String, in context: NSManagedObjectContext) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CPost.mr_countOfEntities(with: predicate, in: context) > 0 {
            return true
        }
        return false
    }

    @discardableResult
    internal func createOrUpdateCPost(_ post: Post, in context: NSManagedObjectContext) -> CPost {
        let p: CPost
        if let _p = getCPostWithId(post.id, in: context) {
            p = _p
            deleteConnectedItemsFrom(post: p, in: context)
        } else {
            p = CPost.mr_createEntity(in: context)!
        }
        updatePostContents(p, from: post)
        return p
    }

    internal func postsSubscribedBy(login: String,
                                    batchSize: Int,
                                    step: Int,
                                    in context: NSManagedObjectContext) -> [CPost]? {
        let request = CPost.mr_requestAllSorted(by: "date",
                                                ascending: false,
                                                in: context)        
        request.fetchLimit = batchSize
        if step >= 0 {
            request.fetchOffset = step * batchSize
        }
        return CPost.mr_executeFetchRequest(request) as? [CPost]
    }

    internal func updatePostContents(_ cp: CPost, from p: Post) {
        cp.id = p.id
        cp.name = p.name
        cp.name = p.content
        cp.date = p.date as NSDate
    }

    internal func convert(post p: CPost) -> Post {
        return Post(id: p.id!,
                    name: p.name ?? "",
                    content: p.content ?? "",
                    date: p.date! as Date)
    }

    internal func getCPostWithId(_ id: String, in context: NSManagedObjectContext) -> CPost? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CPost.mr_findFirst(with: predicate, in: context)
    }

    internal func getCPostWithId(_ id: String) -> CPost? {
        return getCPostWithId(id, in: getContext())
    }

    private func deleteConnectedItemsFrom(post p: CPost, in context: NSManagedObjectContext) {
    }
    
    internal func delete(cpost p: CPost, in context: NSManagedObjectContext) {
        deleteConnectedItemsFrom(post: p, in: context)
        p.mr_deleteEntity(in: context)
    }

    internal func delete(postId id: String, in context: NSManagedObjectContext) {
        if let p = getCPostWithId(id, in: context) {
            delete(cpost: p, in: context)
        }
    }
}

// part of CacheFetchRequestsService
extension Cache {
    func fetchDataControllerForPost() -> FetchManagerProtocol {
        let context = NSManagedObjectContext.mr_default()
        let request = CPost.mr_requestAllSorted(by: "date",
                                                ascending: false,
                                                //with: predicate,
                                                in: context)
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = const.szBatch
        let frc = NSFetchedResultsController(fetchRequest: request,
                                             managedObjectContext: context,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil) as! NSFetchedResultsController<CPost>
        return FetchManager(fetchedResultsController: frc,
                            converter: self)
    }
}
