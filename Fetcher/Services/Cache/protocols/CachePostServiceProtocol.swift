//
//  CachePostServiceProtocol.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 11.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import Foundation

protocol CachePostServiceProtocol {
    func postExistsWith(id: String) -> Bool
    func getPostWith(id: String) -> Post?
    func postsSubscribedBy(login: String,
                           batchSize: Int,
                           step: Int) -> [Post]
    func create(posts: [Post], completion: @escaping (() -> Void))
    func update(posts: [Post], completion: @escaping (() -> Void))
    func delete(postIds: [String], completion: @escaping (() -> Void))
    func deleteAllPosts(completion: @escaping (() -> Void))
}
