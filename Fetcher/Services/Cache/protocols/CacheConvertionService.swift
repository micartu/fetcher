//
//  CacheConvertionService.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 21/10/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol CacheConvertionService {
    func convert(object: Any) -> Any
}
