//
//  CacheFetchRequestsService.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 22/10/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol CacheFetchRequestsService {
    func fetchDataControllerForPost() -> FetchManagerProtocol
}
