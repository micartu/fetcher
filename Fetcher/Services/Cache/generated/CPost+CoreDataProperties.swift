//
//  CPost+CoreDataProperties.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 07/10/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//
//

import Foundation
import CoreData


extension CPost {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CPost> {
        return NSFetchRequest<CPost>(entityName: "CPost")
    }

    @NSManaged public var content: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var name: String?

}
