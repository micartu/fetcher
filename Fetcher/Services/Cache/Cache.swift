//
//  Cache.swift
//  Fetcher
//
//  Created by Michael on 09/07/2019.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import Foundation
import MagicalRecord

typealias CacheCompletion = (() -> Void)
typealias CacheTask = ((NSManagedObjectContext) -> Void)

struct CacheCompletionStruct {
    let task: CacheTask
    let completion: CacheCompletion
}

final class Cache {
    init(secret: SecretServiceProtocol) {
        self.secret = secret
        self.contexts = [String:NSManagedObjectContext]()
        self.contextDates = [String:Date]()
        self.blocks = [CacheCompletionStruct]()
    }

    // MARK: - Contexts

    internal func getContext() -> NSManagedObjectContext {
        if Thread.isMainThread {
            return NSManagedObjectContext.mr_default()
        } else {
            let id = threadId()
            return get(context: id)
        }
    }

    internal func get(context id: String)  -> NSManagedObjectContext {
        let ctx: NSManagedObjectContext
        lock.lock()
        // update/add date of context expiration
        contextDates[id] = Date(timeIntervalSinceNow: const.expireContext)
        if let c = contexts[id] {
            ctx = c
        } else {
            let coordinator = NSPersistentStoreCoordinator.mr_default()!
            ctx = NSManagedObjectContext.mr_context(with: coordinator)
            contexts[id] = ctx
        }
        lock.unlock()
        DispatchQueue(label: "ctx-background").async { [weak self] in
            self?.garbageCollectionForSavedContexts()
        }
        return ctx
    }

    internal func garbageCollectionForSavedContexts() {
        var keys = [String]()
        lock.lock()
        for (k, ctx) in contextDates {
            if ctx < Date() {
                keys.append(k)
            }
        }
        for k in keys {
            contextDates.removeValue(forKey: k)
            contexts.removeValue(forKey: k)
        }
        lock.unlock()
    }

    internal func modify(_ block: @escaping ((NSManagedObjectContext) -> Void),
                         completion: @escaping (() -> Void)) {
        lock.lock(); defer { lock.unlock() }
        if gatherTogether {
            let execute = CacheCompletionStruct(task: block,
                                                completion: completion)
            blocks.append(execute)
        } else {
            MagicalRecord.save({ context in
                block(context)
            }, completion: { (success, err) in
                completion()
            })
        }
    }

    internal let secret: SecretServiceProtocol
    internal var contexts: [String:NSManagedObjectContext]
    internal var blocks: [CacheCompletionStruct]
    internal var contextDates: [String:Date]
    internal let lock = NSRecursiveLock()
    internal let kDelimiter: Character = "|"
    internal let kDefBatchSize = 100
    internal var gatherTogether = false

    struct const {
        static let expireContext: TimeInterval = 5
        static let szBatch = 20
    }
}
