//
//  FileSaverProtocol.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 15.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

enum FileTypes {
    case image
    case smallImage
    case file
    case background(category: String)
    case sticker
}

protocol FileSaverProtocol {
    @discardableResult
    func save(_ type: FileTypes, name: String, with data: Data) -> String
    func contentsOf(type: FileTypes, name: String) -> Data?
    func load(image: String) -> UIImage?
    func loadSmall(image: String) -> UIImage?
    func remove(type: FileTypes, name: String)
}
