//
//  FileSaver.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 15.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class FileSaver {
    // MARK: - Private
    private func path(type: FileTypes, name: String, create: Bool) -> URL {
        var d: String
        var dir: FileManager.SearchPathDirectory = .applicationSupportDirectory
        switch type {
        case .image:
            d = "img"
        case .smallImage:
            d = "small_img"
        case .file:
            d = "cachedfiles"
        case .background(let category):
            d = ("backgrounds" as NSString).appendingPathComponent(category)
        case .sticker:
            d = "stickers"
            dir = .documentDirectory
        }
        let fm = FileManager.default
        let cachesurl = try! fm.url(for: dir,
                                    in: .userDomainMask,
                                    appropriateFor: nil,
                                    create: true)
        let l: String
        if name.contains("/") { // is it a path
            l = (name as NSString).deletingLastPathComponent
        }
        else { // or just a name?
            l = name
        }
        let dirPath = cachesurl.appendingPathComponent(d).appendingPathComponent(l)
        if create {
            do {
                try fm.createDirectory(at: dirPath,
                                       withIntermediateDirectories: true,
                                       attributes: nil)
            }
            catch {
                print("FileSaver: cannot create directory at path name: \(dirPath)")
            }
        }
        return dirPath.appendingPathComponent((name as NSString).lastPathComponent)
    }

    private func loadImageOf(type: FileTypes, name: String) -> UIImage? {
        if let d = contentsOf(type: type, name: name) {
            return UIImage(data: d)
        }
        return nil
    }
}

extension FileSaver: FileSaverProtocol {
    @discardableResult
    func save(_ type: FileTypes, name: String, with data: Data) -> String {
        let fileName = path(type: type, name: name, create: true)
        do {
            try data.write(to: fileName)
        }
        catch {
            print("FileSaver: Cannot create file of \(type) with name: \(name)")
        }
        return fileName.path
    }

    func contentsOf(type: FileTypes, name: String) -> Data? {
        let filePath = path(type: type, name: name, create: false)
        return try? Data(contentsOf: filePath)
    }

    func load(image: String) -> UIImage? {
        return loadImageOf(type: .image, name: image)
    }

    func loadSmall(image: String) -> UIImage? {
        return loadImageOf(type: .smallImage, name: image)
    }

    func remove(type: FileTypes, name: String) {
        let filePath = path(type: type, name: name, create: false)
        let fm = FileManager.default
        if !fm.fileExists(atPath: filePath.path) { return }
        do {
            try fm.removeItem(at: filePath)
        }
        catch {
            print("FileSaver: Cannot delete a file of \(type) with name: \(name); error: '\(error.localizedDescription)'")
        }
        let dirPath = filePath.deletingLastPathComponent()
        let file = filePath.lastPathComponent
        let dir = dirPath.lastPathComponent
        if file == dir {
            do {
                try fm.removeItem(at: dirPath)
            }
            catch {
                print("FileSaver: Cannot delete a directory of file of \(type) with name: \(name); error: '\(error.localizedDescription)'")
            }
        }
    }
}
