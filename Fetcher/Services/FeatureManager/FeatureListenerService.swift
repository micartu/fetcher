//
//  FeatureListenerService.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 28.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol FeatureListenerService {
    func toggled(enabled: Bool)
    func name() -> String
}
