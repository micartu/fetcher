//
//  FeatureManagerService.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 28.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol FeatureManagerService {
    func enableFeaturesFrom(config: [String:Bool])
    func toggle(feature: String, enable: Bool)
    func isEnabled(feature: String) -> Bool
    func append(listener: FeatureListenerService)
    func removeListeners()
}
