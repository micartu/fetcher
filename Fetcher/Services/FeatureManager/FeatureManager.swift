//
//  FeatureManager.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 28.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public class FeatureManager: FeatureManagerService {
    public init() {
    }

    public func enableFeaturesFrom(config: [String:Bool]) {
        for (k, v) in config {
            features[k] = v
            notify(listener: k, enabled: v)
        }
    }

    public func toggle(feature: String, enable: Bool) {
        features[feature] = enable
        notify(listener: feature, enabled: enable)
    }

    public func isEnabled(feature: String) -> Bool {
        if let f = features[feature] {
            return f
        }
        return false
    }

    public func append(listener: FeatureListenerService) {
        let en = isEnabled(feature: listener.name())
        listeners.append(listener)
        listener.toggled(enabled: en)
    }

    public func removeListeners() {
        listeners.removeAll()
    }

    // MARK: - Private

    private func notify(listener: String, enabled: Bool) {
        for l in listeners {
            if l.name() == listener {
                l.toggled(enabled: enabled)
                break
            }
        }
    }

    private var listeners = [FeatureListenerService]()
    private var features = [String:Bool]()
}
