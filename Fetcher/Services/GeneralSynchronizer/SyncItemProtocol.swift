//
//  SyncItemProtocol.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 25.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol SyncItemProtocol {
    var itemID: String { get }
    var tag: String { get }
}
