//
//  GeneralSynchronizer.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 25.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

typealias StatusDictType = [String:[String:String]]

/// preforms synchronization of elements in two given sorted arrays
/// maintains a helping status-set for that
final class GeneralSynchronizer {
    let saveStatus: (([SyncItemProtocol], StatusDictType) -> Void)
    var status: SortedArray<SyncItemProtocol>
    var statusDict: StatusDictType

    init(statusItems: [SyncItemProtocol],
         statusDict: StatusDictType,
         deb: ConsoleDebuggerService?,
         saveStatus: @escaping (([SyncItemProtocol], StatusDictType) -> Void)) {
        self.saveStatus = saveStatus
        self.statusDict = statusDict
        self.deb = deb
        status = SortedArray(sorted: statusItems, areInIncreasingOrder: const.sortOrder)
    }

    /// Synchroinzes contents of A and B arrays
    ///
    /// - Parameter A array of elements with copying/deleting operations on them
    /// - Precondition: the items in `A` and `B` must be in sorted order: a binary search algorithm is performed on them
    func sync(A: ItemsToSyncProtocol, B: ItemsToSyncProtocol) {
        // search through each element in A
        var sA = SortedArray(sorted: A.items, areInIncreasingOrder: const.sortOrder)
        var sB = SortedArray(sorted: B.items, areInIncreasingOrder: const.sortOrder)
        // synchronize first array with second one
        partSync(sA: &sA, sB: &sB, A: A, B: B, tagA: const.tagA, tagB: const.tagB)
        // then synchronize second array with first one
        partSync(sA: &sB, sB: &sA, A: B, B: A, tagA: const.tagB, tagB: const.tagA)
        // save changed status-array for future use
        saveStatus(status.elements(), statusDict)
    }

    private func partSync(sA: inout SortedArray<SyncItemProtocol>,
                          sB: inout SortedArray<SyncItemProtocol>,
                          A: ItemsToSyncProtocol,
                          B: ItemsToSyncProtocol,
                          tagA: String,
                          tagB: String) {
        deb?.pr("-- start part sync tagA: \(tagA) tagB: \(tagB)")
        let g = DispatchGroup()
        for a in sA {
            if sB.contains(a) {
                if !status.contains(a) {
                    // it exists on side A and side B,
                    // but not in status,
                    // add the ID to the status
                    deb?.pr("-- A + B - status: item id: \(a.itemID) tag: \(a.tag)")
                    status.insert(a)
                    let bi = sB.anyIndex(of: a)!
                    let b = sB[bi]
                    // A - B - status
                    resolveConflict(sA: &sA,
                                    sB: &sB,
                                    A: A,
                                    B: B,
                                    a: a,
                                    b: b)
                } else {
                    //  the item exists on A, B, status
                    if let ld = statusDict[a.itemID] {
                        let ltA = ld[tagA]!
                        let ltB = ld[tagB]!
                        let bi = sB.anyIndex(of: a)!
                        let b = sB[bi]
                        deb?.pr("-- A + B + status: item id: \(a.itemID) tag: \(a.tag)")
                        deb?.pr("-- tagA: \(ltA) tagB: \(ltB); a id: \(a.itemID) tag: \(a.tag) b id: \(b.itemID) tag: \(b.tag)")
                        if ltA != a.tag && ltB == b.tag {
                            // If the tag has changed on A but not B, copy from A to B
                            deb?.pr("-- replace b id: \(b.itemID) tag: \(b.tag) with a id: \(a.itemID) tag: \(a.tag)")
                            sB.remove(b)
                            sB.insert(a)
                            g.enter()
                            B.replace(a, at: bi) {
                                g.leave()
                            }
                            g.wait()
                            let d = [tagA: ltA, tagB: ltA]
                            statusDict[a.itemID] = d
                        } else if ltA == a.tag && ltB != b.tag {
                            // If the tag has changed on B but not A, copy from B to A.
                            deb?.pr("-- replace a id: \(a.itemID) tag: \(a.tag) with b id: \(b.itemID) tag: \(b.tag)")
                            sA.remove(a)
                            sA.insert(b)
                            g.enter()
                            A.replace(b, at: bi) {
                                g.leave()
                            }
                            g.wait()
                            let d = [tagA: ltB, tagB: ltB]
                            statusDict[a.itemID] = d
                        } else if ltA != a.tag && ltB != b.tag {
                            resolveConflict(sA: &sA,
                                            sB: &sB,
                                            A: A,
                                            B: B,
                                            a: a,
                                            b: b)
                        }
                    } else {
                        let d = [tagA: a.tag, tagB: a.tag]
                        statusDict[a.itemID] = d
                    }
                }
            } else {
                if status.contains(a) {
                    // the ID exists on side A and the status,
                    // but not on B, it has been deleted on B.
                    // Delete it from A and the status
                    deb?.pr("-- A - B + status: item id: \(a.itemID) tag: \(a.tag)")
                    sA.remove(a)
                    g.enter()
                    A.remove(a) {
                        g.leave()
                    }
                    g.wait()
                    status.remove(a)
                    statusDict.removeValue(forKey: a.itemID)
                } else {
                    // the ID exists on side A,
                    // but not on B or the status,
                    // it must have been created on A.
                    // Copy the item from A to B and also insert it into status
                    deb?.pr("-- A - B - status: item id: \(a.itemID) tag: \(a.tag)")
                    let i = sB.insert(a)
                    g.enter()
                    B.insert(a, at: i) {
                        g.leave()
                    }
                    g.wait()
                    status.insert(a)
                    let d = [tagA: a.tag, tagB: a.tag]
                    statusDict[a.itemID] = d
                }
            }
        }
    }

    public func getSortOrder() -> ((SyncItemProtocol, SyncItemProtocol) -> Bool) {
        return const.sortOrder
    }

    private func resolveConflict(sA: inout SortedArray<SyncItemProtocol>,
                                 sB: inout SortedArray<SyncItemProtocol>,
                                 A: ItemsToSyncProtocol,
                                 B: ItemsToSyncProtocol,
                                 a: SyncItemProtocol,
                                 b: SyncItemProtocol) {
        deb?.pr("-- resolve conflict a id: \(a.itemID) tag: \(a.tag); b id: \(b.itemID) tag: \(b.tag)")
        let g = DispatchGroup()
        if a.tag > b.tag {
            deb?.pr("-- resolved conflict a id: \(a.itemID) tag: \(a.tag)")
            sB.remove(b)
            let i = sB.insert(a)
            g.enter()
            B.replace(a, at: i) {
                g.leave()
            }
            g.wait()
            let d = [const.tagA: a.tag, const.tagB: a.tag]
            statusDict[a.itemID] = d
        } else if a.tag < b.tag {
            deb?.pr("-- resolved conflict b id: \(b.itemID) tag: \(b.tag)")
            sA.remove(a)
            let i = sA.insert(b)
            g.enter()
            A.replace(b, at: i) {
                g.leave()
            }
            g.wait()
            let d = [const.tagA: b.tag, const.tagB: b.tag]
            statusDict[a.itemID] = d
        } else {
            // same contents
            let d = [const.tagA: a.tag, const.tagB: a.tag]
            statusDict[a.itemID] = d
            deb?.pr("-- resolved conflict; same contents")
        }
    }

    private struct const {
        static let sortOrder: ((SyncItemProtocol, SyncItemProtocol) -> Bool) = { $0.itemID < $1.itemID }
        static let tagA = ":stagA"
        static let tagB = ":stagB"
    }

    private let deb: ConsoleDebuggerService?
}
