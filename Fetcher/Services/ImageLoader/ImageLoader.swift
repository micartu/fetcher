//
//  ImageLoader.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 15.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

final class ImageLoader {
    init(disk: FileSaverProtocol) {
        //self.transport = transport
        self.disk = disk
    }

    // MARK: Private
    private let disk: FileSaverProtocol
    private var errs = [String:Int]()
    private var lock = NSRecursiveLock()

    private struct const {
        static let maxErr = 4
        static let waitTime: TimeInterval = 0.5
    }
}

extension ImageLoader: ImageLoaderProtocol {
    func loadOffline(image: String) -> UIImage? {
        return nil
    }

    func errableLoad(image: String, completion: @escaping ImageLoaderErrCompletion) {
        if image.count == 0 { return }
        // TODO: delete in production (after tests):
        if let asset = UIImage(named: image) {
            completion(asset, nil)
        } else {
            let e = NSError(domain: Const.domain,
                            code: -1,
                            userInfo: [
                                NSLocalizedDescriptionKey: "Cannot find image in library".localized
                ])
            completion(nil, e)
        }
    }

    func load(image: String, completion: @escaping ImageLoaderCompletion) {
        errableLoad(image: image) { (im, err) in
            if let image = im {
                completion(image)
            }
        }
    }

    // TODO: remove it after you've got methods on server's side which gives you
    // a smaller version of an image
    private func resizeIfNeeded(image: UIImage) -> UIImage {
        let width = UIScreen.main.bounds.width
        if image.size.width > width {
            let h = heightFrom(width: width, proportional: image.size)
            let im = imageWith(image: image,
                               scaledToSize:  CGSize(width: width, height: h))
            return im
        }
        return image
    }

    func errorableLoadOf(_ image: String, completion: @escaping ImageLoaderErrCompletion) {
        let e = NSError(domain: Const.domain,
                        code: -1,
                        userInfo: [
                            NSLocalizedDescriptionKey: "Not implemented".localized
            ])
        completion(nil, e)        
    }
}
