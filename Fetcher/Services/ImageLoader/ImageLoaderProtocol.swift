//
//  ImageLoaderProtocol.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 15.07.19.
//  Copyright © 2019 RusMobileContent. All rights reserved.
//

import UIKit

typealias ImageLoaderErrCompletion =  ((UIImage?, NSError?) -> Void)
typealias ImageLoaderCompletion =  ((UIImage) -> Void)

protocol ImageLoaderProtocol: class {
    func errorableLoadOf(_ image: String, completion: @escaping ImageLoaderErrCompletion)
    func loadOffline(image: String) -> UIImage?
    func load(image: String, completion: @escaping ImageLoaderCompletion)
}
