//
//  NetFun+Post.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 20/11/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import UIKit

extension NetFun: NetPostsService {
    func wallPostsWith(offset: Int,
                       count: Int,
                       success: (([Post]) -> Void)?,
                       failure: ((NSError) -> Void)?) {
        // limit the available pages
        guard offset > 0 && offset < 5 else { return }
        let startId = count * (offset - 1) + 1
        success?(generatePosts(idStart: startId, count: count))
    }
    
    private func generatePosts(idStart: Int, count: Int) -> [Post] {
        var pp = [Post]()
        for i in idStart..<(idStart + count) {
            let id = "\(i)"
            let n = "Barny #\(i)"
            let c = "Wall #\(i)"
            let p = Post(id: id,
                         name: n,
                         content: c,
                         date: Date())
            pp.append(p)
        }
        return pp
    }
}
