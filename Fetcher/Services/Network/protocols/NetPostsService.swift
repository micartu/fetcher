//
//  NetPostsService.swift
//  Fetcher
//
//  Created by Michael Artuerhof on 20/11/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

protocol NetPostsService {
    func wallPostsWith(offset: Int,
                       count: Int,
                       success: (([Post]) -> Void)?,
                       failure: ((NSError) -> Void)?)
}
