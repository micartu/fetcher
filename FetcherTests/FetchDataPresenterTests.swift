//
//  FetchDataPresenterTests.swift
//  LearnFetchRequestTests
//
//  Created by Michael Artuerhof on 20/11/2019.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import XCTest
@testable import Fetcher
let kTimeout: TimeInterval = 5

class FetchDataPresenterTests: XCTestCase {
    var cache: Cache!
    var presenter: TFetcher?
    
    private struct const {
        static let sz = 20
    }
    
    override func setUp() {
        let secret: SecretServiceProtocol = ServicesAssembler.inject()
        cache = Cache(secret: secret)
    }

    override func tearDown() {
        presenter = nil
    }
    
    func preparePresenter(net: TFetcherNetwork) -> (TFetcher, TInterator) {
        let tm: ThemeManagerProtocol = ServicesAssembler.inject()
        let presenter = TFetcher(manager: cache.fetchDataControllerForPost(),
                                 batchSize: const.sz)
        let interactor = TInterator(cache: cache, net: net)
        presenter.netInteractor = interactor
        presenter.theme = tm.getCurrentTheme()
        self.presenter = presenter
        return (presenter, interactor)
    }
    
    func testDataConsistency() {
        let exp = self.expectation(description: "data sorting is the same as it's on network")
        // empty cache + first data load from network
        cache.deleteAllPosts {
            let net = TFetcherNetwork()
            let recs = net.loadDataFor(offset: 0, count: const.sz)
            self.cache.create(posts: recs) {
                let rs = self.cache.postsSubscribedBy(login: "",
                                                      batchSize: const.sz,
                                                      step: 0)
                XCTAssert(rs.count == recs.count,
                          "but it was rs: \(rs.count); recs.count: \(recs.count)")
                for i in 0..<min(rs.count, recs.count) {
                    XCTAssert(rs[i].id == recs[i].id)
                }
                self.cache.deleteAllPosts {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testFirstRun() {
        let exp = self.expectation(description: "first load data check")
        // empty cache + first data load from network
        cache.deleteAllPosts {
            let net = TFetcherNetwork()
            let (presenter, interactor) = self.preparePresenter(net: net)
            presenter.getContents()
            delay(1.0) {
                // all records from page 1 should be in db
                let mcount = presenter.modelsCount(for: 0)
                XCTAssert(mcount == const.sz,
                          "but it was: \(mcount)")
                // same test but loads data directly from cache:
                interactor.fetchEntities(page: 1,
                                         count: const.sz) { ents, err in
                                            XCTAssert(ents.count == const.sz,
                                                      "but it was: \(ents.count)")
                                            self.cache.deleteAllPosts {
                                                exp.fulfill()
                                            }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
    
    func testRefreshRecords() {
        // add n records, then move to the last one at the bottom
        let exp = self.expectation(description: "reload data check")
        // empty cache + first data load from network
        cache.deleteAllPosts {
            let net = TFetcherNetwork()
            let n = 4
            // assume we do not have n first records (they are not available yet):
            net.recordsOffset = n
            let (presenter, interactor) = self.preparePresenter(net: net)
            presenter.getContents()
            delay(1) {
                XCTAssert(presenter.modelsCount(for: 0) != 0)
                interactor.fetchEntities(page: 1, count: 1) { ents, err in
                    XCTAssert(ents.count == 1)
                    // then after some time those n posts were available on server's side:
                    net.recordsOffset = 0
                    // try to refresh data:
                    presenter.refreshContents()
                    delay(1) {
                        let d = net.loadDataFor(offset: 1, count: const.sz)
                        let netData = d[d.startIndex..<d.startIndex.advanced(by: min(n, d.count))]
                        interactor.fetchEntities(page: 1, count: n, completion: { ents, err in
                            XCTAssert(ents.count == n && ents.count == netData.count)
                            XCTAssert(ents.first?.itemID == netData.first?.itemID)
                            XCTAssert(ents.last?.itemID == netData.last?.itemID)
                            self.cache.deleteAllPosts {
                                exp.fulfill()
                            }
                        })
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
    
    func testRunOverTheLastRecord() {
        let exp = self.expectation(description: "load next page of data check")
        // 1. empty cache
        // 2. load first page from network
        // 3. move to the last record
        // 4. check if the next page would be loaded
        cache.deleteAllPosts {
            let net = TFetcherNetwork()
            let (presenter, _) = self.preparePresenter(net: net)
            presenter.getContents()
            delay(1) {
                // all records from page 1 should be in db
                let mcount = presenter.modelsCount(for: 0)
                XCTAssert(mcount == const.sz,
                          "but it was: \(mcount)")
                // move to the last record:
                _ = presenter.model(for: IndexPath(row: mcount - 1, section: 0))
                // wait a little:
                delay(1) {
                    // check if data of next page is loaded:
                    let mcountChanged = presenter.modelsCount(for: 0)
                    XCTAssert(mcountChanged == 2 * const.sz,
                              "but it was: \(mcountChanged)")
                    self.cache.deleteAllPosts {
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}

class TFetcher: FetchedNetPresenter {
    init(manager: FetchManagerProtocol, batchSize: Int) {
        super.init(manager: manager, batchSize: batchSize)
    }
}

class TFetcherNetwork {
    var recordsOffset = 0
    
    func loadDataFor(offset: Int, count: Int) -> [Post] {
        let kDate: TimeInterval = 453381071
        let id = recordsOffset + offset * count + 1
        return generatePosts(idStart: id,
                             count: count,
                             dateStart: kDate)
    }
}

class TInterator: FetchedNetInteractorInput {
    let cache: CachePostServiceProtocol
    let net: TFetcherNetwork
    
    init(cache: CachePostServiceProtocol, net: TFetcherNetwork) {
        self.cache = cache
        self.net = net
    }
    
    func wrap(entity: Any, with theme: Theme) -> CellAnyModel {
        let p = entity as? Post ?? Post(id: "",
                                        name: "",
                                        content: "",
                                        date: Date())
        return TPostModel(theme: theme,
                          id: p.id,
                          content: p.content)
    }
    
    func loadOverNetwork(page: Int, count: Int,
                         completion: @escaping (([SyncItemProtocol], NSError?) -> Void)) {
        let data = net.loadDataFor(offset: page, count: count)
        completion(data, nil)
    }
    
    func fetchEntities(page: Int, count: Int,
                       completion: @escaping (([SyncItemProtocol], NSError?) -> Void)) {
        let p = page - 1 // our page starts from 0 (but in 'network' it starts from 1)!
        let data = cache.postsSubscribedBy(login: "", batchSize: count, step: p)
        completion(data, nil)
    }
    
    // MARK: ItemsToSyncActionsProtocol
    
    func insert(_ item: SyncItemProtocol,
                at index: Int,
                completion: @escaping (() -> Void)) {
        if let p = item as? Post {
            cache.create(posts: [p], completion: completion)
        }  else {
            completion()
        }
    }
    
    func replace(_ item: SyncItemProtocol,
                 at index: Int,
                 completion: @escaping (() -> Void)) {
        if let p = item as? Post {
            cache.update(posts: [p], completion: completion)
        } else {
            completion()
        }
    }
    
    func remove(_ item: SyncItemProtocol,
                completion: @escaping (() -> Void)) {
        if let p = item as? Post {
            cache.delete(postIds: [p.id], completion: completion)
        } else {
            completion()
        }
    }
}

func generatePosts(idStart: Int, count: Int, dateStart: TimeInterval) -> [Post] {
    return (idStart..<(idStart + count))
        .map({
            Post(id: "\($0)",
                 name: "\($0)",
                 content: "content of \($0)",
                 date: Date(timeIntervalSince1970: dateStart - TimeInterval($0)))
        })
}

struct TPostModel: CommonCell {
    let theme: Theme
    let id: String
    let content: String

    func setup(cell: WallPostCell) {
        cell.id = id
    }
}
