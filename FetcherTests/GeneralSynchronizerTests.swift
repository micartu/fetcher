//
//  GeneralSynchronizerTests.swift
//  FetcherTests
//
//  Created by Michael Artuerhof on 25.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import XCTest
@testable import Fetcher

class GeneralSynchronizerTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testGeneralSync() {
        let countOfSyncedItems = 9
        let aId = [0, 1, 2, 3, 5, 6, 7, 8].map({ genSyncable(id: "\($0)") })
        let bId = [1, 3, 4, 5].map({ genSyncable(id: "\($0)") })
        let syncer = GeneralSynchronizer(statusItems: [],
                                         statusDict: [:],
                                         deb: nil) { status, dict in
                                            XCTAssert(status.count == countOfSyncedItems,
                                                      "status.count: \(status.count)")
        }
        let A = FakeItemsToSync(items: aId)
        let B = FakeItemsToSync(items: bId)
        syncer.sync(A: A, B: B)
        XCTAssert(A.items.count == B.items.count)
        for i in A.items.indices {
            let a = A.items[i]
            let b = B.items[i]
            XCTAssert(a.itemID == b.itemID)
        }
    }

    func testSyncWithDifferentContents() {
        var aId = [1, 2, 3, 5].map({ genSyncable(id: "\($0)") })
        var bId = [1, 3, 4, 5].map({ genSyncable(id: "\($0)") })
        // same id (= 1), but different and conflicting content:
        let kTag = "14" // more relevant tag
        aId[0].tag = "10"
        bId[0].tag = kTag
        let syncer = GeneralSynchronizer(statusItems: [],
                                         statusDict: [:],
                                         deb: nil) { status, dict in
        }
        let A = FakeItemsToSync(items: aId)
        let B = FakeItemsToSync(items: bId)
        syncer.sync(A: A, B: B)
        XCTAssert(A.items.count == B.items.count)
        for i in A.items.indices {
            let a = A.items[i]
            let b = B.items[i]
            XCTAssert(a.itemID == b.itemID)
        }
        XCTAssert(A.items[0].tag == kTag, "but it was: \(A.items[0])")
    }
}

func genSyncable(id: String) -> Syncable {
    return Syncable(itemID: id, tag: id)
}

struct Syncable: SyncItemProtocol {
    let itemID: String
    var tag: String
}

class FakeItemsToSync: ItemsToSyncProtocol {
    var items: [SyncItemProtocol]

    init(items: [SyncItemProtocol]) {
        self.items = items
    }

    func insert(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        items.insert(item, at: index)
        completion()
    }

    func replace(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        remove(item) { [weak self] in
            self?.insert(item, at: index) {
                completion()
            }
        }
    }

    func remove(_ item: SyncItemProtocol, completion: @escaping (() -> Void)) {
        items.removeAll(where: { $0.itemID == item.itemID })
        completion()
    }
 }
